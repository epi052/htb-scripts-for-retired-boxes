# HTB Scripts for Retired Boxes

Just a place to share some things I've written while participating in Hack The Box.

1. [Tartarsauce inotify solution](/tartarsauce)
2. [Jerry - Tomcat default credentials login](/jerry)
3. [Smasher - Web Exploit/Padding Oracle/Race Condition](/smasher)
4. [Reddish - Multiple Scripts](/reddish)
5. [Zipper - Zabbix API RCE tool](/zipper)
6. [Frolic - Buffer Overflow Script](/frolic)
7. [Curling - Golang HTTP Fileserver/Dirty-Sock Payload Generator](/curling)
